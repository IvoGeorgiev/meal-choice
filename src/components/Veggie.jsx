import { useEffect, useState } from "react";
import styled from "styled-components";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/splide.min.css";
import { Link } from "react-router-dom";

function Veggie() {
  const [veggie, setVeggie] = useState([]);
  const [numCards, setNumCards] = useState(4); // Keep track of the number of cards to show

  useEffect(() => {
    getVeggie();
  }, []);

  useEffect(() => {
    // Update the number of cards to show based on screen size
    const handleResize = () => {
      if (window.innerWidth < 768) {
        setNumCards(1);
      } else if (window.innerWidth < 1124) {
        setNumCards(2);
      } else if (window.innerWidth < 1500) {
        setNumCards(3);
      } else {
        setNumCards(4);
      }
    };
    window.addEventListener("resize", handleResize);
    handleResize(); // Call once initially to set the number of cards based on initial screen size
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  const getVeggie = async () => {
    const api = await fetch(
      "https://api.spoonacular.com/recipes/random?apiKey=cc8e7e73e2e245fc93c753170233b1e0&number=9&tags=vegetarian"
    );
    const data = await api.json();
    setVeggie(data.recipes);
  };

  return (
    <div>
      <Wrapper>
        <h3>Vegetarian Picks</h3>
        <Splide
          options={{
            perPage: numCards, // Use the numCards state to set the number of cards to show
            arrows: true,
            pagination: false,
            drag: "free",
            gap: "5rem",
          }}
        >
          {veggie.slice(0, 9).map((recipe) => {
            // Only show the first numCards recipes
            return (
              <SplideSlide key={recipe.id}>
                <Card>
                  <Link to={"/recipe/" + recipe.id}>
                    <p>{recipe.title}</p>
                    <img src={recipe.image} alt={recipe.title} />
                    <Gradient />
                  </Link>
                </Card>
              </SplideSlide>
            );
          })}
        </Splide>
      </Wrapper>
    </div>
  );
}

const Wrapper = styled.div`
  margin: 4rem 0rem;
`;

const Card = styled.div`
  min-height: 20rem;
  border-radius: 2rem;
  overflow: hidden;
  position: relative;

  img {
    border-radius: 2rem;
    position: absolute;
    left: 0;
    object-fit: cover;
    display: block;
    width: 100%;
    height: 100%;
    }
  }
  P{
    position: absolute;
    z-index: 10;
    left: 50%;
    bottom: 0%;
    transform: translate(-50%, 0%);
    color: white;
    width: 100%;
    text-align: center;
    font-weight: 600;
    font-size: 1rem;
    height: 40%;
    display: flex;
    justify-content: center;
    align-items: center;
    }
`;

const Gradient = styled.div`
  z-index: 3;
  position: absolute;
  width: 100%;
  height: 100%;
  background: linear-gradient(rgba(0, 0, 0, 0), rgba(0, 0, 0, 0.5));
`;

export default Veggie;
