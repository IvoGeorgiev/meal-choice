import React, { useEffect, useState } from "react";
import styled from "styled-components";
// import { motion } from "framer-motion";
import { Link, useParams } from "react-router-dom";

function Searched() {
  const [searchedRecipes, setSearchedRecipes] = useState([]);
  let params = useParams();

  const getSearched = async (name) => {
    const fetchLink =
      "https://api.spoonacular.com/recipes/complexSearch?apiKey=cc8e7e73e2e245fc93c753170233b1e0&query=" +
      name;
    console.log(fetchLink);
    const data = await fetch(fetchLink);
    const recipes = await data.json();
    setSearchedRecipes(recipes.results);
  };
  // eslint-disable-next-line
  useEffect(() => {
    // eslint-disable-next-line
    getSearched(params.search);
    // eslint-disable-next-line
  }, [params.search]);

  return (
    <Grid>
      {searchedRecipes.map((item) => {
        return (
          <Card key={item.id}>
            <Link to={"/recipe/" + item.id}>
              <img src={item.image} alt="" />
              <h4>{item.title}</h4>
            </Link>
          </Card>
        );
      })}
    </Grid>
  );
}

const Grid = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(20rem, 1fr));
  grid-gap: 3rem;
`;

const Card = styled.div`
  img {
    width: 100%;
    border-radius: 2rem;
  }
  a {
    text-decoration: none;
  }
  h4 {
    text-align: center;
    padding: 1rem;
  }
`;

export default Searched;
