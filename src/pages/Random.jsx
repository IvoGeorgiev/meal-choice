import { useEffect, useState } from "react";
import styled from "styled-components";
import React from "react";

function Random() {
  const [details, setDetails] = useState({});
  const [activeTab, setActiveTab] = useState("ingredients");

  const fetchDetails = async () => {
    try {
      const data = await fetch(
        "https://api.spoonacular.com/recipes/random?apiKey=cc8e7e73e2e245fc93c753170233b1e0&number=1"
      );
      const detailData = await data.json();
      setDetails(detailData.recipes[0]);
    } catch (error) {
      console.error(error);
    }
  };
  // eslint-disable-next-line
  useEffect(() => {
    // eslint-disable-next-line
    fetchDetails();
    // eslint-disable-next-line
  }, []);
  return (
    <div>
      <ButtonContainer>
        <Button onClick={fetchDetails}>Get a new recipe</Button>
      </ButtonContainer>

      <DetailWrapper>
        <div>
          <h2>{details.title}</h2>
          <img src={details.image} alt="" />
        </div>
        <Info>
          <Button
            className={activeTab === "ingredients" ? "active" : ""}
            onClick={() => setActiveTab("ingredients")}
          >
            Ingredients
          </Button>
          <Button
            className={activeTab === "instructions" ? "active" : ""}
            onClick={() => setActiveTab("instructions")}
          >
            Instructions
          </Button>
          {activeTab === "ingredients" && details.extendedIngredients && (
            <ul>
              {details.extendedIngredients.map((ingredient) => (
                <li key={ingredient.id}>{ingredient.original}</li>
              ))}
            </ul>
          )}
          {activeTab === "instructions" && (
            <div>
              <h3 dangerouslySetInnerHTML={{ __html: details.summary }}></h3>
              <h3
                dangerouslySetInnerHTML={{ __html: details.instructions }}
              ></h3>
            </div>
          )}
        </Info>
      </DetailWrapper>
    </div>
  );
}

const DetailWrapper = styled.div`
  margin-top: 5rem;
  margin-bottom: 5rem;
  display: flex;
  flex-direction: row;
  flex-shrink: 1;
  div {
    flex-size: 1fr;
  }

  @media only screen and (max-width: 720px) {
    flex-direction: column;
    align-items: flex-start;
    margin-top: 2rem;
    max-width: 30rem;
  }
  // .img {
  //   @media only screen and (max-width: 720px) {
  //     max-width: 30rem;
  // }
  img {
    //min-width: 10rem;
    @media only screen and (max-width: 720px) {
      max-width: 350px;
    }
  }

  .active {
    background: linear-gradient(35deg, #494949, #313131);
    color: white;
  }
  h2 {
    margin-bottom: 2rem;
  }
  h3 {
    @media only screen and (max-width: 720px) {
      font-size: 0.8rem;
    }
  }
  li {
    font-size: 1.2rem;
    line-height: 2.5rem;
  }
  ul {
    margin-top: 2rem;
  }
`;
const ButtonContainer = styled.div`
  display: flex;
  justify-content: center;
  margin-bottom: 1rem;
  margin-top: 3rem;
`;

const Button = styled.button`
  padding: 1rem 2rem;
  color: #313131;
  background: white;
  border: 2px solid black;
  margin-right: 2rem;
  font-weight: 600;

  @media only screen and (max-width: 720px) {
    margin-top: 2rem;
    margin-bottom: 2rem;
    margin-right: 1rem;
  }
`;

const Info = styled.div`
  margin-left: 10rem;
  flex-size: 1fr;
  @media only screen and (max-width: 720px) {
    margin-left: 0rem;
  }
`;

export default Random;
